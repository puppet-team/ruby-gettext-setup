ruby-gettext-setup (0.34-2) unstable; urgency=medium

  * debian/patches:
    - Make dependency on ruby-gettext less strict in gemspec. Upstream
      recently changed this to declare that only ruby-gettext << 3.3.0 is
      acceptable, as more modern versions lack support for Ruby << 2.5. That
      is not of concern to us at this point in time, as we are already
      transitioning to Ruby 2.7.

 -- Georg Faerber <georg@debian.org>  Wed, 05 Feb 2020 17:47:13 +0000

ruby-gettext-setup (0.34-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    - Bump Standards-Version to 4.5.0, no changes required.
  * debian/patches:
    - Refresh for new release.

 -- Georg Faerber <georg@debian.org>  Tue, 04 Feb 2020 10:53:46 +0000

ruby-gettext-setup (0.31-1) unstable; urgency=medium

  * New upstream version 0.31.
  * Set LANG to C.UTF-8 before running the tests, to make them work.
  * Drop compat file, rely on debhelper-compat.
  * Use my debian.org mail address.
  * Bump Standards-Version to 4.4.0.
  * Declare that the build doesn't need root.
  * Make the build verbose.
  * Drop obsolete patch to fix test errors and refresh the remaining ones.
  * Add Salsa CI config.

 -- Georg Faerber <georg@debian.org>  Sat, 31 Aug 2019 22:55:04 +0000

ruby-gettext-setup (0.30-2) unstable; urgency=medium

  * d/watch: Migrate to gemwatch.d.n and use HTTPS
  * d/upstream: Added Upstream metadata
  * d/patches: Fix UTF-8 issue during spec tests (Closes: #894829)
    Thanks Hleb Valoshka for this patch!
    https://lists.debian.org/debian-ruby/2018/04/msg00034.html
  * d/control: Bump to Standards-Version 4.1.4 (no changes needed)

 -- Sebastien Badia <sbadia@debian.org>  Sat, 05 May 2018 20:53:05 +0200

ruby-gettext-setup (0.30-1) unstable; urgency=medium

  [ Georg Faerber ]
  * New upstream release. (Closes: #892737)
  * debian/changelog: Remove never uploaded version 0.25-1.
  * debian/compat: Bump debhelper compat level to 11.
  * debian/control:
    - Add myself as uploader.
    - Bump required debhelper version to >= 11~.
    - Use salsa.debian.org for Vcs-{Git,Browser}.
    - Bump Standards-Version to 4.1.3, no changes needed.
    - Update build dependencies according to new upstream version: Add
      getttext package.
  * debian/copyright:
    - Use HTTPS in link to copyright format spec.
    - Bump years and update copyright info for Debian packaging.
  * debian/patches:
    - Refresh for Debian release 0.30-1.
    - Add patch to use 'require' instead of 'require_relative', as used by
      upstream. This enables running the specs via autopkgtest. This patch
      supersedes an existing one, which was dropped, accordingly.

  [ Sebastien Badia ]
  * debian/control:
    - Use own Debian email address.
    - Update build dependencies according to new upstream version: Add git and
      ruby-locale packages.

  [ Apollon Oikonomopoulos ]
  * d/rules: unexport LANGUAGE; fixes test failures

 -- Georg Faerber <georg@riseup.net>  Tue, 13 Mar 2018 02:10:37 +0100

ruby-gettext-setup (0.7-1) unstable; urgency=medium

  * [6627a04] New upstream version 0.7
  * [5ed56a9] Update dependencies
  * [14035cb] Remove obsolete patch replace-git-ls-with-static-list
  * [8bd2edd] Add patch loosen_dependencies

 -- Markus Frosch <lazyfrosch@debian.org>  Sun, 18 Sep 2016 18:45:52 +0200

ruby-gettext-setup (0.3-2) unstable; urgency=medium

  * [a59f77d] add patch to fix autopkgtest runs (Closes: #835630)
  * [4b7d074] replace git-ls with static file list

 -- Michael Moll <kvedulv@kvedulv.de>  Tue, 13 Sep 2016 22:21:26 +0200

ruby-gettext-setup (0.3-1) unstable; urgency=medium

  * Initial release (Closes: #831668)

 -- Sebastien Badia <seb@sebian.fr>  Mon, 18 Jul 2016 12:19:57 +0200
